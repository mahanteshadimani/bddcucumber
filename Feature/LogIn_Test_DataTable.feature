@FunctionalTest
Feature: Login Action

 @RegressionTest
Scenario Outline: Successful Login with Valid Credentials
 Given User is on Home Page
 When User Navigate to LogIn Page
 And User enters "<username>" and "<password>"
 Then Message displayed Login Successfully
Examples:
    | username   | password |
    | mahantesh | Automation@123 |
    | testuser_2 | Test@153 |
    
 @RegressionTest
Scenario: Successful LogOut
 When User LogOut from the Application
 Then Message displayed LogOut Successfully