package cucumberTest;

import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
 
@RunWith(Cucumber.class)
@CucumberOptions(
 features = "Feature"
 ,glue={"stepDefinition"},
 plugin = { "pretty","html:cucumberReport","json:cucumberReport/cucumber.json" },
 
 monochrome = true,
 tags={"@FunctionalTest","~@RegressionTest"}
 //, ORed
 //"" Anded
 //~ ignore the tagged test cases
 	

 )
 
public class TestRunner {
 
}
